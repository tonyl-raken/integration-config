import { APIGatewayProxyHandler } from "aws-lambda";
import { ddbDocClient } from "./libs/ddb-doc-client";

const TABLE_NAME = process.env.TABLE_NAME || '';

export const handler: APIGatewayProxyHandler = async () => {
  const params = {
    TableName: TABLE_NAME
  };

  try {
    const response = await ddbDocClient.scan(params).promise();
    return { statusCode: 200, body: JSON.stringify(response.Items) };
  } catch (dbError) {
    return { statusCode: 500, body: JSON.stringify(dbError) };
  }
};
