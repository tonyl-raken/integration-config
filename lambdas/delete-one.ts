import { APIGatewayProxyHandler } from "aws-lambda";
import { ddbDocClient } from "./libs/ddb-doc-client";

const TABLE_NAME = process.env.TABLE_NAME || '';
const PRIMARY_KEY = process.env.PRIMARY_KEY || '';

export const handler: APIGatewayProxyHandler = async (event) => {

  const requestedItemId = event.pathParameters?.id;
  if (!requestedItemId) {
    return { statusCode: 400, body: `Error: You are missing the path parameter id` };
  }

  const params = {
    TableName: TABLE_NAME,
    Key: {
      [PRIMARY_KEY]: requestedItemId
    }
  };

  try {
    await ddbDocClient.delete(params).promise();
    return { statusCode: 200, body: '' };
  } catch (dbError) {
    return { statusCode: 500, body: JSON.stringify(dbError) };
  }
};
