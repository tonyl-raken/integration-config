import fetch from 'node-fetch';
import { APIGatewayAuthorizerResult, APIGatewayTokenAuthorizerEvent } from "aws-lambda";

/**
 * Custom authorizer to validate token via raken-api
 * @param event - API Gateway token authorizer event
 */
export const handler = async (event: APIGatewayTokenAuthorizerEvent): Promise<APIGatewayAuthorizerResult> => {
  const authToken: string = event.authorizationToken;
  try {
    const response = await fetch(`${process.env.AUTH_URL}`, {
      method: 'GET',
      headers: {
        'Authentication': authToken,
      }
    });
    const data = (await response.json()) as { role: string };
    console.log(`User data: ${JSON.stringify(data, null, 2)}`);
    const result = {
      principalId: 'user',
      policyDocument: {
        Version: '2012-10-17',
        Statement: [
          {
            Action: 'execute-api:Invoke',
            Effect: data.role === 'ROLE_SUPER_USER' ? 'allow' : 'deny',
            Resource: event.methodArn.split("/")[0] + "/*",
          },
        ],
      },
    };
    console.log(`Result: ${JSON.stringify(result, null, 2)}`);
    return result;
  } catch (e) {
    console.error(e);
    throw new Error('Unauthorized');
  }
};
