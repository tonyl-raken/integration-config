import * as AWS from 'aws-sdk';

const ddbDocClient = new AWS.DynamoDB.DocumentClient({ region: "us-west-2" });

export { ddbDocClient }
