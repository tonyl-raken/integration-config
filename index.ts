import {
  AuthorizationType,
  JsonSchemaType,
  LambdaIntegration,
  MethodLoggingLevel,
  Model,
  RequestValidator,
  RestApi,
  TokenAuthorizer
} from '@aws-cdk/aws-apigateway';
import { AttributeType, Table } from '@aws-cdk/aws-dynamodb';
import { Runtime } from '@aws-cdk/aws-lambda';
import { App, Duration, RemovalPolicy, Stack } from '@aws-cdk/core';
import { NodejsFunction, NodejsFunctionProps } from '@aws-cdk/aws-lambda-nodejs';
import { join } from 'path'
import { MethodOptions } from "@aws-cdk/aws-apigateway/lib/method";
import { RetentionDays } from "@aws-cdk/aws-logs";

const stackName = 'ICStack';

export class IntegrationConfigurationStack extends Stack {
  constructor(app: App, id: string) {
    super(app, id);

    const dynamoTable = new Table(this, 'items', {
      partitionKey: {
        name: 'itemId',
        type: AttributeType.STRING
      },
      tableName: `${stackName}-items`,

      /**
       *  The default removal policy is RETAIN, which means that cdk destroy will not attempt to delete
       * the new table, and it will remain in your account until manually deleted. By setting the policy to
       * DESTROY, cdk destroy will delete the table (even if it has data in it)
       */
      removalPolicy: RemovalPolicy.DESTROY, // NOT recommended for production code
    });

    const nodeJsFunctionProps: NodejsFunctionProps = {
      bundling: {
        externalModules: [
          'aws-sdk', // Use the 'aws-sdk' available in the Lambda runtime
        ],
        minify: false,
      },
      depsLockFilePath: join(__dirname, 'lambdas', 'package-lock.json'),
      environment: {
        PRIMARY_KEY: 'itemId',
        TABLE_NAME: dynamoTable.tableName,
        AUTH_URL: 'https://alpha-priv.rakenapp.com/ra/user/data',
      },
      runtime: Runtime.NODEJS_14_X,
      timeout: Duration.seconds(15),
      memorySize: 128,
      /**
       *  The default log retention setting is RetentionDays.INFINITE
       */
      logRetention: RetentionDays.ONE_WEEK,
    }

    // Create a Lambda function for each of the CRUD operations
    const authorizerLambda = new NodejsFunction(this, 'authorizerFunction', {
      functionName: `${stackName}-authorizerFunction`,
      entry: join(__dirname, 'lambdas', 'authorizer.ts'),
      ...nodeJsFunctionProps,
    });
    const getOneLambda = new NodejsFunction(this, 'getOneItemFunction', {
      functionName: `${stackName}-getOneItemFunction`,
      entry: join(__dirname, 'lambdas', 'get-one.ts'),
      ...nodeJsFunctionProps,
    });
    const getAllLambda = new NodejsFunction(this, 'getAllItemsFunction', {
      functionName: `${stackName}-getAllItemsFunction`,
      entry: join(__dirname, 'lambdas', 'get-all.ts'),
      ...nodeJsFunctionProps,
    });
    const createOneLambda = new NodejsFunction(this, 'createItemFunction', {
      functionName: `${stackName}-createItemFunction`,
      entry: join(__dirname, 'lambdas', 'create.ts'),
      ...nodeJsFunctionProps,
    });
    const updateOneLambda = new NodejsFunction(this, 'updateItemFunction', {
      functionName: `${stackName}-updateItemFunction`,
      entry: join(__dirname, 'lambdas', 'update-one.ts'),
      ...nodeJsFunctionProps,
    });
    const deleteOneLambda = new NodejsFunction(this, 'deleteItemFunction', {
      functionName: `${stackName}-deleteItemFunction`,
      entry: join(__dirname, 'lambdas', 'delete-one.ts'),
      ...nodeJsFunctionProps,
    });

    // Custom authorizer to validate token via raken-api
    const rakenApiCustomAuthorizer = new TokenAuthorizer(this, 'rakenApiCustomAuthorizer', {
      handler: authorizerLambda,
      resultsCacheTtl: Duration.minutes(1),
    });

    // Grant the Lambda function read access to the DynamoDB table
    dynamoTable.grantReadData(getAllLambda);
    dynamoTable.grantReadData(getOneLambda);
    dynamoTable.grantReadWriteData(createOneLambda);
    dynamoTable.grantReadWriteData(updateOneLambda);
    dynamoTable.grantReadWriteData(deleteOneLambda);

    // Integrate the Lambda functions with the API Gateway resource
    const getAllIntegration = new LambdaIntegration(getAllLambda);
    const createOneIntegration = new LambdaIntegration(createOneLambda);
    const getOneIntegration = new LambdaIntegration(getOneLambda);
    const updateOneIntegration = new LambdaIntegration(updateOneLambda);
    const deleteOneIntegration = new LambdaIntegration(deleteOneLambda);

    // Create an API Gateway resource for each of the CRUD operations
    const api = new RestApi(this, 'integrationConfigApi', {
      restApiName: 'Integration Configuration Service',
      description: 'Integration Configuration Service',
      deployOptions: {
        stageName: "v1",
        loggingLevel: MethodLoggingLevel.INFO,
      }
    });

    // Model
    const createItemRequestModel = new Model(this, "create-item-request-model", {
      restApi: api,
      contentType: "application/json",
      description: "Create item request model",
      modelName: "CreateItemRequestModel",
      schema: {
        type: JsonSchemaType.OBJECT,
        required: ["itemValue"],
        properties: {
          itemValue: { type: JsonSchemaType.STRING },
        },
      },
    });

    // Method option
    const methodOptions: MethodOptions = {
      authorizationType: AuthorizationType.CUSTOM,
      authorizer: rakenApiCustomAuthorizer,
    };

    // Path /items
    const items = api.root.addResource('items');
    // GET /items
    items.addMethod('GET', getAllIntegration, methodOptions);
    // POST /items
    items.addMethod('POST', createOneIntegration, {
      ...methodOptions,
      requestValidator: new RequestValidator(
        this,
        "create-item-request-body-validator",
        {
          restApi: api,
          requestValidatorName: "create-item-request-body-validator",
          validateRequestBody: true,
        }
      ),
      requestModels: {
        "application/json": createItemRequestModel,
      }
    });

    // Path /items/{id}
    const singleItem = items.addResource('{id}');
    // GET /items/{id}
    singleItem.addMethod('GET', getOneIntegration, methodOptions);
    // PATCH /items/{id}
    singleItem.addMethod('PATCH', updateOneIntegration, methodOptions);
    // DELETE /items/{id}
    singleItem.addMethod('DELETE', deleteOneIntegration, methodOptions);
  }
}

const app = new App();
new IntegrationConfigurationStack(app, 'IntegrationConfigurationStack');
app.synth();
